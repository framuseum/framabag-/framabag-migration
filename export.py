#!/usr/bin/env python3
import csv

import os
import sqlite3
import json

import sys

from Entry import Entry, serialiseur_perso


def export_entries(name, collection, basepath):
    filename = basepath + "/json/" + str(name) + ".json"
    with open(filename, "w") as fichier:
        json.dump(collection, fichier, default=serialiseur_perso)
    print("Exported file " + filename)


def file_exists(name, basepathData):
    return os.path.isfile(basepathData + '/u/' + name + '/db/poche.sqlite')


def fetch_entries(path, basepathData, basepathExport):

    if not file_exists(path, basepathData):
        print("Database file for" + str(path) + " does not exists")
        raise IOError("File doesn't exist", path)

    conn = sqlite3.connect(basepathData + '/u/' + path + '/db/poche.sqlite')
    conn.text_factory = str

    t = conn.cursor()
    t.execute("SELECT name FROM sqlite_master WHERE name='tagsentries' and type='table'")

    has_tags = t.fetchone()

    c = conn.cursor()

    if not has_tags:
        c.execute('SELECT e.id, e.title, e.url, e.is_read, e.is_fav, e.content from entries e')

        entries = c.fetchall()

        entries_collection = []

        for entry in entries:
            entryobj = Entry(entry[0], entry[1], entry[2], entry[3], entry[4], entry[5])
            entries_collection.append(entryobj)

        export_entries(path, entries_collection, basepathExport)

    else:
        c.execute('SELECT e.id, e.title, e.url, e.is_read, e.is_fav, e.content, t.value from entries e left join tags_entries te on e.id = te.entry_id left join tags t on te.tag_id = t.id')

        entries = c.fetchall()

        entry_previous = None
        entries_collection = []
        for entry in entries:
            if entry_previous is not None and entry[0] != entry_previous.id:
                entryobj = Entry(entry[0], entry[1], entry[2], entry[3], entry[4], entry[5])
                entry_previous = entryobj
                if entry[6] is not None:
                    entryobj.addtag(entry[6])
                entries_collection.append(entryobj)
            elif entry_previous is not None and entry[0] == entry_previous.id and entry[6] is not None:
                entry_previous.addtag(entry[6])
            else:
                entryobj = Entry(entry[0], entry[1], entry[2], entry[3], entry[4], entry[5])
                entry_previous = entryobj
                if entry[6] is not None:
                    entryobj.addtag(entry[6])
                entries_collection.append(entryobj)
        conn.close()
        export_entries(path, entries_collection, basepathExport)


basepathDataArg = sys.argv[1]
basepathExportArg = sys.argv[2]
sqliteErrors = []
i = 0
with open('accounts.csv', newline='') as csvfile:
    readersum = csv.reader(csvfile, delimiter=',', quotechar='"')
    somme = sum(1 for row in readersum)
    print(str(somme) + " accounts to process...")

with open('accounts.csv', newline='') as csvfile:
    reader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in reader:
        print("Exporting entries...")
        try:
            fetch_entries(row[0], basepathDataArg, basepathExportArg)
        except sqlite3.OperationalError as err:
            print("Error while retrieving entries. Adding " + row[0] + "to the list of errored accounts")
            sqliteErrors.append({'type': 'sqlite', 'account': row[0], 'msg': str(err)})
        except IOError as err:
            print("IO Error for " + row[0])
            sqliteErrors.append({'type': 'io', 'account': row[0], 'msg': str(err)})
        with open('errors-export.json', 'w') as fp:
            json.dump(sqliteErrors, fp)
        i += 1
        print("Account " + str(i+1) + " on " + str(somme) + " processed")

with open('errors-export.json', 'w') as fp:
    json.dump(sqliteErrors, fp)
