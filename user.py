import json
import string
import random
import os
import csv
import sys


def create_account(username, email):
    password = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(10))
    os.system("bin/console fos:user:create '" + username + "' '" + email + "' " + password + " --env=prod")


path2v1 = sys.argv[1]
errors = []
i = 0
with open('accounts.csv', newline='') as csvfile:
    readersum = csv.reader(csvfile, delimiter=',', quotechar='"')
    somme = sum(1 for row in readersum)
    print(str(somme) + " accounts to process...")

with open('accounts.csv', newline='') as csvfile:
    reader = csv.reader(csvfile, delimiter=',', quotechar='"')
    for row in reader:
        print(row[0] + " " + row[1])
        if os.path.exists(path2v1 + '/u/' + row[0]):
            create_account(row[0], row[1])
            print("Created account for user " + str(i) + "/" + str(somme))
        else:
            print("no directory exists for this user.")
            errors.append(row[0])


with open('errors-users.json', 'w') as fp:
    json.dump(errors, fp)
